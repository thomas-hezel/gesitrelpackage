/* zazudesign – die Schwarzwald Werbeagentur (c) JavaScript 2017-2020
author: Thomas Hezel, Berlin 20180509
Leaflet Map JavaScript - variable lati and longi coming from FLUID
*/

//in jquery click call for leaflet js
$('#activateMapBtn').click(function() {

var map = L.map('map', {zoomControl: false}).setView([lati, longi], 8);

  /*above remove control and put a new one on bottomleft*/
  map.addControl( L.control.zoom({position: 'bottomleft'}));

/*tiles from mapBox
    L.tileLayer('https://{s}.tiles.mapbox.com/v3/uhradone.ija63bia/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18
}).addTo(map);
*/

/*tiles from openStreetMap*/
 L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a>',
    maxZoom: 18
}).addTo(map);



   var toothOrt1 = L.icon({
    iconUrl: '/fileadmin/Resources/Public/Icons/leaf-green.png',
    shadowUrl: '/fileadmin/Resources/Public/Icons/leaf-shadow.png',

    iconSize:     [38, 95], // size of the icon
    shadowSize:   [78, 84], // size of the shadow
    iconAnchor:   [19, 95], // point of the icon which will correspond to marker's location
    shadowAnchor: [19, 84],  // the same for the shadow
    popupAnchor:  [-6, -76] // point from which the popup should open relative to the iconAnchor
});

var toothOrt2 = L.icon({
    iconUrl: '/fileadmin/Resources/Public/Icons/leaf-green.png',
    shadowUrl: '/fileadmin/Resources/Public/Icons/leaf-shadow.png',

    iconSize:     [38, 95], // size of the icon
    shadowSize:   [78, 84], // size of the shadow
    iconAnchor:   [19, 95], // point of the icon which will correspond to marker's location
    shadowAnchor: [19, 84],  // the same for the shadow
    popupAnchor:  [-6, -76] // point from which the popup should open relative to the iconAnchor
});

var toothOrt3 = L.icon({
    iconUrl: '/fileadmin/Resources/Public/Icons/leaf-green.png',
    shadowUrl: '/fileadmin/Resources/Public/Icons/leaf-shadow.png',

    iconSize:     [38, 95], // size of the icon
    shadowSize:   [78, 84], // size of the shadow
    iconAnchor:   [19, 95], // point of the icon which will correspond to marker's location
    shadowAnchor: [19, 84],  // the same for the shadow
    popupAnchor:  [-6, -76] // point from which the popup should open relative to the iconAnchor
});


  var greenIcon = L.icon({
    iconUrl: '/fileadmin/Resources/Public/Icons/leaf-green.png',
    shadowUrl: '/fileadmin/Resources/Public/Icons/leaf-shadow.png',

    iconSize:     [38, 95], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

L.marker([lati1, longi1], {icon: toothOrt1}).addTo(map).bindPopup("GESITREL AG<br /><b>Schaffhausen</b>");
L.marker([lati2, longi2], {icon: toothOrt2}).addTo(map).bindPopup("GESITREL AG<br /><b>Basel</b>");
L.marker([lati3, longi3], {icon: toothOrt3}).addTo(map).bindPopup("GESITREL AG<br /><b>Bern</b>");

//jquery  additions
$('.dataWarning').addClass('hide');
$('#map').addClass('dontShowBackground');


});


