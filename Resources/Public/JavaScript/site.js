/* zazudesign – die Schwarzwald Werbeagentur (c) JavaScript 20181124
author: Thomas Hezel, zazudesign - Berlin
main site JS for responsive Design 4col
*/

// Avoid `console` errors in browsers that lack a console. From Boilerplate.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());


// -----  zazu site js  ---------

$(document).ready(function() {

/*page-slide Navigation*/
$(".open").pageslide();

//class for mobile nav no-js-hook in html
$('html').removeClass('no-js').addClass('js');

/*drop down for mainNav in Normal Frame*/
$('nav li ul').removeClass('fallback').hide();
$('nav li').hover(
  function () {
    $('ul', this).stop().slideDown(200);
  },
  function () {
    $('ul', this).stop().slideUp(200);
  }
);

/*=== Störungshotline ausklappen =====*/
var area = $("#hotline");
var shade = $('#shader');

$('#hotlineBtn').click(function(){
 if(area.css('max-width') == "0px") {
    $(window).scrollTop(0);
    area.animate({'max-width':'400px','max-height':'1300px'},1000);
    shade.addClass('shade');
    }else{
     area.animate({'max-width':'0','max-height':'0'},1000);
     shade.removeClass('shade');
    };
});

$('#closeHotline').click(function(){
     area.animate({'max-width':'0','max-height':'0'},1000);
     shade.removeClass('shade');
});


/*==== cookies -- deactivated google analytics will only be loaded on second click======
$('#tx_cookies_accept input').click(
function() {
setTimeout(
function() {
location.reload();
},
200);
});

*/




//DCE 50 fadeIn fadeOut blue cover over image

	$('.d50Element').hover(
	function() {
		$('.d50Shader', this).fadeIn(1000);
	},
	function() {
		$('.d50Shader', this).fadeOut(1000);
	}
		);


//end document ready
});


//opt-out-link Datenschutzerklärung bei Google

     // GOOGLE OPT_OUT_COOKIE  Set to the same value as the web property used on the site
     var gaProperty = 'UA-15305486-1';

      // Disable tracking if the opt-out cookie exists.
     var disableStr = 'ga-disable-' + gaProperty;
    if (document.cookie.indexOf(disableStr + '=true') > -1) {
     window[disableStr] = true;
     }

     // Opt-out function
     function gaOptout() {
        document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
         window[disableStr] = true;

       //zazu addition to work with cookie Extension and set it through this link off
       document.cookie = 'tx_cookies_accepted=1; path=/';
       location.reload();
      }




