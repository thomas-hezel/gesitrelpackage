<?php
defined('TYPO3') || die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['zazu'] = 'EXT:gesitrelpackage/Configuration/RTE/Default-RTE-package.yaml';

/***************
 * PageTS
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:gesitrelpackage/Configuration/TsConfig/Page/All.tsconfig">');
