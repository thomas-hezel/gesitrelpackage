<?php
/**
 * Created by zazu.berlin
 * Author: thomas hezel
 * Date: 2019-05-09 / 20200601
 * This adds automatically to static templates in root template
 * all Typoscript that is in the TypoScript folder
 */

defined('TYPO3') || die();

call_user_func(function()
{
    /**
     * Extension key
     */
    $extensionKey = 'gesitrelpackage';

    /**
     * Default TypoScript
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        $extensionKey,
        'Configuration/TypoScript',
        'gesitrelpackage'
    );

});
